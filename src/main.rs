use std::{
    fs::{self, DirEntry},
    collections::{BTreeMap, BTreeSet, HashSet},
    io::Write,
    path::{Path, PathBuf},
};

use anyhow::Result;
use buffered_reader::BufferedReader;
use clap::Parser;

use sequoia_openpgp as openpgp;
use openpgp::{
    Cert,
    crypto::hash::Digest,
    Fingerprint,
    KeyID,
    cert::prelude::*,
    packet::{prelude::*, key::*},
    parse::Parse,
    serialize::{Serialize, stream::*},
    types::*,
};
use sequoia_ipc as ipc;
use ipc::Keygrip;

#[derive(Debug, Default)]
struct Scan {
    artifacts: BTreeMap<PathBuf, Artifact>,
    fingerprints: BTreeSet<Fingerprint>,
    keyids: BTreeSet<KeyID>,
    keygrips: HashSet<Keygrip>,
    merged_certs: BTreeMap<Fingerprint, Cert>,
}

impl Scan {
    fn find_certs(&mut self, dir: &Path) -> Result<()> {
        visit_dirs(dir, &mut |e: &DirEntry| -> Result<()> {
            if e.path().extension().and_then(|o| o.to_str()) == Some(".new")
                || e.path().extension().and_then(|o| o.to_str()) == Some(".elc") {
                return Ok(());
            }

            let mut printed_path = false;

            if let Ok(parser) = CertParser::from_file(e.path()) {
                let mut acc = Vec::new();

                for certo in parser {
                    if ! printed_path {
                        eprintln!("{}:", e.path().display());
                        printed_path = true;
                    }

                    match certo {
                        Ok(cert) => {
                            println!("   - {}", cert.fingerprint());
                            for key in cert.keys() {
                                let fp = key.fingerprint();
                                if ! key.primary() {
                                    println!("     - {}", fp);
                                }
                                self.keyids.insert(fp.clone().into());
                                self.fingerprints.insert(fp);
                                if let Ok(g) = Keygrip::of(key.mpis()) {
                                    self.keygrips.insert(g);
                                }
                            }

                            use std::collections::btree_map::Entry::*;
                            match self.merged_certs.entry(cert.fingerprint()) {
                                Vacant(v) => {
                                    v.insert(cert.clone());
                                },
                                Occupied(mut v) => {
                                    let merged =
                                        v.get().clone().merge_public_and_secret(
                                            cert.clone())?;
                                    v.insert(merged);
                                },
                            }

                            acc.push(cert);
                        },
                        Err(err) => {
                            eprintln!("  - a cert failed to parse: {}", err);
                        },
                    }
                }

                if ! acc.is_empty() {
                    let mut f = buffered_reader::File::open(e.path())?;
                    let armored = f.data_hard(1).map(|d| d[0] & 0x80 == 0)
                        .unwrap_or(false);

                    self.artifacts.insert(e.path().into(), Artifact {
                        value: Value::Keyring(acc, armored),
                        patches: vec![],
                    });
                }
            }

            Ok(())
        })
    }

    fn into_mapper(self) -> Result<Mapper> {
        fn short_keyid(k: &KeyID) -> String {
            k.to_string().chars().skip(8).collect()
        }

        let mut from =
            self.fingerprints.iter().map(ToString::to_string)
            .chain(self.keygrips.iter().map(ToString::to_string))
            .chain(self.keyids.iter().map(ToString::to_string))
            .chain(self.keyids.iter().map(short_keyid))
            .collect::<Vec<_>>();
        from.sort();

        let from_casei = from.iter().map(|f| {
            let mut e = String::new();
            for c in f.chars() {
                if c.is_ascii_alphabetic() {
                    e.push('[');
                    e.push_str(&c.to_lowercase().to_string());
                    e.push_str(&c.to_uppercase().to_string());
                    e.push(']');
                } else {
                    e.push(c);
                }
            }
            e
        }).collect::<Vec<_>>();

        let re = regex::bytes::Regex::new(&from_casei.join("|")).unwrap();

        let mut obsolete_certs = Vec::new();
        let mut updated_certs = Vec::new();
        let mut substitutions = BTreeMap::<Vec<u8>, openpgp::Packet>::default();
        let mut text_substitutions = BTreeMap::<Vec<u8>, Vec<u8>>::default();

        for cert in self.merged_certs.values() {
            // XXX: skip good certs

            obsolete_certs.push(cert.fingerprint());
            let mut acc: Vec<Packet> = Vec::new();
            let mut primary: Key<_, PrimaryRole> =
                Key4::generate_ecc(true, Curve::Ed25519)?.into();
            primary.set_creation_time(cert.primary_key().creation_time())?;

            let mut primary_signer = primary.clone().into_keypair()?;
            substitutions.insert(hash_key(cert.primary_key().key()),
                                 primary.clone().into());
            text_substitutions.insert(
                cert.primary_key().fingerprint().to_string().into(),
                primary.fingerprint().to_string().into());
            text_substitutions.insert(
                cert.primary_key().keyid().to_string().into(),
                primary.keyid().to_string().into());
            text_substitutions.insert(
                short_keyid(&cert.primary_key().keyid()).into(),
                primary.keyid().to_string().into());
            if let Ok(old_grip) = Keygrip::of(cert.primary_key().mpis()) {
                text_substitutions.insert(
                    old_grip.to_string().into(),
                    Keygrip::of(primary.mpis()).expect("you'd better")
                        .to_string().into());
            }

            acc.push(primary.clone().into());
            let new_cert = Cert::from_packets(acc.clone().into_iter())?;

            for uid in cert.userids() {
                acc.push(uid.userid().clone().into());

                for sig in uid.self_signatures() {
                    let builder = SignatureBuilder::from(sig.clone())
                        .set_signature_creation_time(
                            sig.signature_creation_time().expect("has one"))?
                        .set_features(Features::empty().set_mdc())?
                        .set_preferred_hash_algorithms(vec![
                            HashAlgorithm::SHA512,
                            HashAlgorithm::SHA256,
                        ])?
                        .set_preferred_symmetric_algorithms(vec![
                            SymmetricAlgorithm::AES256,
                            SymmetricAlgorithm::AES128,
                        ])?;

                    let binding =
                        uid.bind(&mut primary_signer, &new_cert, builder)?;
                    substitutions.insert(hash_packet(sig),
                                         binding.clone().into());
                    acc.push(binding.into());
                }

                for sig in uid.self_revocations() {
                    let builder = SignatureBuilder::from(sig.clone())
                        .set_signature_creation_time(
                            sig.signature_creation_time().expect("has one"))?;
                    let binding =
                        uid.bind(&mut primary_signer, &new_cert, builder)?;
                    substitutions.insert(hash_packet(sig),
                                         binding.clone().into());
                    acc.push(binding.into());
                }
            }

            // XXX: user attributes

            for key in cert.keys().subkeys() {
                // Is this an encryption key?  Check!
                let for_encryption =
                    key.pk_algo() == PublicKeyAlgorithm::ElGamalEncrypt
                    || key.self_signatures().any(|s| {
                        s.key_flags().map(
                            |f| f.for_storage_encryption()
                                || f.for_transport_encryption())
                            .unwrap_or(false)
                    });

                let mut subkey: Key<_, SubordinateRole> =
                    Key4::generate_ecc(! for_encryption,
                                       if for_encryption {
                                           Curve::Cv25519
                                       } else {
                                           Curve::Ed25519
                                       })?.into();
                subkey.set_creation_time(key.creation_time())?;

                let mut subkey_signer = subkey.clone().into_keypair()?;
                substitutions.insert(hash_key(key.key()),
                                     subkey.clone().into());

                text_substitutions.insert(
                    key.fingerprint().to_string().into(),
                    subkey.fingerprint().to_string().into());
                text_substitutions.insert(
                    key.keyid().to_string().into(),
                    subkey.keyid().to_string().into());
                text_substitutions.insert(
                    short_keyid(&key.keyid()).into(),
                    subkey.keyid().to_string().into());
                if let Ok(old_grip) = Keygrip::of(key.mpis()) {
                    text_substitutions.insert(
                        old_grip.to_string().into(),
                        Keygrip::of(subkey.mpis()).expect("you'd better")
                            .to_string().into());
                }

                acc.push(subkey.clone().into());

                for sig in key.self_signatures() {
                    let mut builder = SignatureBuilder::from(sig.clone())
                        .set_signature_creation_time(
                            sig.signature_creation_time().expect("has one"))?;

                    // Make key flags explicit if they were implicit
                    // before.
                    if builder.key_flags().is_none() {
                        if for_encryption {
                            builder = builder.set_key_flags(
                                KeyFlags::empty()
                                    .set_transport_encryption()
                                    .set_storage_encryption())?;
                        } else {
                            builder = builder.set_key_flags(
                                KeyFlags::empty()
                                    .set_signing())?;
                        }
                    }

                    match sig.embedded_signatures().count() {
                        0 => (), // It's gonna be fine... will it?
                        1 => {
                            let backsig =
                                SignatureBuilder::new(
                                    SignatureType::PrimaryKeyBinding)
                                .set_signature_creation_time(
                                    sig.embedded_signatures().next().unwrap()
                                        .signature_creation_time()
                                        .expect("has one"))?
                                .sign_primary_key_binding(&mut subkey_signer,
                                                          &primary,
                                                          &subkey)?;
                            builder = builder.set_embedded_signature(backsig)?;
                        },
                        n => give_up(
                            format!("found {} embedded signatures", n))?,
                    }

                    let binding =
                        subkey.bind(&mut primary_signer, &new_cert, builder)?;
                    substitutions.insert(hash_packet(sig),
                                         binding.clone().into());
                    acc.push(binding.into());
                }

                for sig in key.self_revocations() {
                    let builder = SignatureBuilder::from(sig.clone())
                        .set_signature_creation_time(
                            sig.signature_creation_time().expect("has one"))?;
                    let binding =
                        subkey.bind(&mut primary_signer, &new_cert, builder)?;
                    substitutions.insert(hash_packet(sig),
                                         binding.clone().into());
                    acc.push(binding.into());
                }

                // XXX: certifiations, other_revocations
            }

            updated_certs.push(Cert::from_packets(acc.into_iter())?);
        }

        Ok(Mapper {
            scan: self,
            re,
            substitutions,
            text_substitutions,
            obsolete_certs,
            updated_certs,
        })
    }
}

struct Mapper {
    scan: Scan,
    re: regex::bytes::Regex,
    substitutions: BTreeMap::<Vec<u8>, openpgp::Packet>,
    text_substitutions: BTreeMap<Vec<u8>, Vec<u8>>,
    obsolete_certs: Vec<Fingerprint>,
    updated_certs: Vec<Cert>,
}

impl Mapper {
    fn find_identifiers(&mut self, dir: &Path) -> Result<()> {
        visit_dirs(dir, &mut |e: &DirEntry| -> Result<()> {
            let mut found_one = false;
            let mut f = buffered_reader::File::open(e.path())?;
            let d = f.data_eof()?;

            let mut patches = Vec::new();
            for m in self.re.find_iter(d) {
                if ! found_one {
                    eprintln!("{}:", e.path().display());
                    found_one = true;
                }

                patches.push(Patch {
                    start: m.start(),
                    len: m.end() - m.start(),
                });

                eprintln!("  - [{}..{}] = {}",
                          m.start(),
                          m.end(),
                          String::from_utf8_lossy(m.as_bytes()));
            }

            if found_one {
                if self.scan.artifacts.get(&e.path()).is_some() {
                    eprintln!("Already substituted certs in {}, \
                               skipping identifier replacement here for now.",
                              e.path().display());
                    return Ok(());
                }

                self.scan.artifacts.insert(e.path().into(), Artifact {
                    value: Value::Data(f),
                    patches,
                });
            }

            Ok(())
        })?;

        Ok(())
    }

    fn write_new_files(&mut self) -> Result<()> {
        for (path, artifact) in self.scan.artifacts.iter_mut() {
            let mut new_path = path.clone().into_os_string();
            new_path.push(".new");
            let mut sink = fs::File::create(new_path)?;

            match &mut artifact.value {
                Value::Keyring(certs, armored) => {
                    let mut sink = Message::new(sink);

                    if *armored {
                        let has_secrets = certs.iter().any(|c| c.is_tsk());

                        sink = Armorer::new(sink)
                            .kind(if has_secrets {
                                openpgp::armor::Kind::SecretKey
                            } else {
                                openpgp::armor::Kind::PublicKey
                            })
                            .build()?;
                    }

                    for p in certs.iter().cloned()
                        .flat_map(|cert| cert.into_packets())
                        .map(|p| match &p {
                            Packet::Signature(s) =>
                                self.substitutions.get(&hash_packet(s))
                                .cloned().unwrap_or(p),
                            Packet::UserID(u) =>
                                self.substitutions.get(&hash_packet(u))
                                .cloned().unwrap_or(p),
                            Packet::UserAttribute(u) =>
                                self.substitutions.get(&hash_packet(u))
                                .cloned().unwrap_or(p),
                            Packet::PublicKey(k) =>
                                self.substitutions.get(&hash_key(k))
                                .cloned()
                                .map(|k| {
                                    // Get rid of the secret.
                                    let k: Key<SecretParts, PrimaryRole> =
                                        k.downcast().expect("matched on type");
                                    Key::<PublicParts, PrimaryRole>::from(k)
                                        .into()
                                })
                                .unwrap_or(p),
                            Packet::PublicSubkey(k) =>
                                self.substitutions.get(&hash_key(k))
                                .cloned()
                                .map(|k| {
                                    // Get rid of the secret.
                                    let k: Key<SecretParts, SubordinateRole> =
                                        k.downcast().expect("matched on type");
                                    Key::<PublicParts, SubordinateRole>::from(k)
                                        .into()
                                })
                                .unwrap_or(p),
                            Packet::SecretKey(k) =>
                                self.substitutions.get(&hash_key(k))
                                .cloned()
                                .unwrap_or(p),
                            Packet::SecretSubkey(k) =>
                                self.substitutions.get(&hash_key(k))
                                .cloned()
                                .unwrap_or(p),
                            _ => p,
                        })
                    {
                        p.serialize(&mut sink)?;
                    }

                    sink.finalize()?;
                },
                Value::Data(f) => {
                    let d = f.data_eof()?;
                    let mut written = 0;
                    for p in &artifact.patches {
                        assert!(written <= p.start);
                        sink.write_all(&d[written..p.start])?;
                        if let Some(replacement) =
                            self.text_substitutions
                            .get(&d[p.start..p.start+p.len])
                        {
                            sink.write_all(replacement.as_slice())?;
                        } else {
                            sink.write_all(&d[p.start..p.start+p.len])?;
                        }
                        written = p.start + p.len;
                    }
                    sink.write_all(&d[written..])?;
                },
            }
        }

        Ok(())
    }

    fn commit(&self) -> Result<()> {
        eprintln!("Committing changes...");
        for path in self.scan.artifacts.keys() {
            let mut new_path = path.clone().into_os_string();
            new_path.push(".new");
            let new_path = PathBuf::from(new_path);
            let mut old_path = path.clone().into_os_string();
            old_path.push(".old");

            if new_path.exists() {
                eprintln!("  - {}", path.display());
                std::fs::rename(path, old_path)?;
                std::fs::rename(new_path, path)?;
            }
        }

        eprintln!();

        let obsolete = self.obsolete_certs.iter().map(ToString::to_string)
            .collect::<Vec<_>>();
        eprintln!("Obsoleted keys: {}\n", obsolete.join(" "));

        let path = "updated-keys.pgp";
        let mut sink = fs::File::create(path)?;
        for c in &self.updated_certs {
            c.as_tsk().serialize(&mut sink)?;
        }
        eprintln!("Updated keys written to {}", path);

        Ok(())
    }
}

#[derive(Debug)]
struct Artifact {
    value: Value,
    patches: Vec<Patch>,
}

#[derive(Debug)]
struct Patch {
    start: usize,
    len: usize,
}

#[derive(Debug)]
enum Value {
    Keyring(Vec<Cert>, bool),
    Data(buffered_reader::File<'static, ()>),
}

/// Helps spot problems with and update OpenPGP data in test suites.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Shall we actually replace the files?
    #[clap(short, long, value_parser)]
    commit: bool,

    #[clap(value_parser)]
    paths: Vec<PathBuf>,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let mut scan = Scan::default();

    // First pass: see if a file contains OpenPGP certs.
    for dir in &args.paths {
        scan.find_certs(dir)?;
    }

    let mut mapper = scan.into_mapper()?;

    // Second pass: scan all files for identifiers for previously
    // discovered objects.
    for dir in &args.paths {
        mapper.find_identifiers(dir)?;
    }

    mapper.write_new_files()?;

    if args.commit {
        mapper.commit()?;
    }

    Ok(())
}

// one possible implementation of walking a directory only visiting files
fn visit_dirs(dir: &Path, cb: &mut dyn FnMut(&DirEntry) -> Result<()>)
              -> Result<()>
{
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                visit_dirs(&path, cb)?;
            } else {
                cb(&entry)?;
            }
        }
    }
    Ok(())
}

fn hash_packet<P: openpgp::crypto::hash::Hash>(p: &P) -> Vec<u8> {
    let mut hash = HashAlgorithm::default().context().expect("mti");
    p.hash(&mut hash);
    hash.into_digest().expect("sha2 is secure")
}

fn hash_key<P: KeyParts, R: KeyRole>(k: &Key<P, R>) -> Vec<u8> {
    match k {
        Key::V4(v) => hash_packet(v),
        _ => unimplemented!(),
    }
}

fn give_up<M: std::fmt::Display>(message: M) -> Result<()> {
    Err(anyhow::anyhow!("Too hard for me: {}", message))
}
